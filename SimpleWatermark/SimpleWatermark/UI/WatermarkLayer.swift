//
//  WatermarkLayer.swift
//  SimpleWatermark
//
//  Created by Aleksey Cherkasskiy on 31/05/2019.
//  Copyright © 2019 alcherk. All rights reserved.
//

import Cocoa

final class WatermarkLayer: CATextLayer {
    private var text: String = "" {
        didSet { updateText(with: text) }
    }
    
    static func make(with text: String, font: NSFont) -> WatermarkLayer {
        let layer = WatermarkLayer()
        layer.font = font
        layer.text = text
        return layer
    }
    
    private override init() {
        super.init()
        
        borderWidth = 1.0
        borderColor = NSColor.red.cgColor
    }
    
    override init(layer: Any) {
        super.init(layer: layer)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func updateText(with newValue: String) {
        let drawFont = font as? NSFont ?? NSFont.systemFont(ofSize: fontSize)
        let stringWidth = newValue.width(withConstrainedHeight: fontSize, font: drawFont)
        frame = CGRect(x: 0, y: 0, width: stringWidth, height: fontSize)
        string = newValue
    }
}

extension String {
    func height(withConstrainedWidth width: CGFloat, font: NSFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        let scale = width / boundingBox.width
        return ceil(boundingBox.height * scale)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: NSFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let context = NSStringDrawingContext()
        context.minimumScaleFactor = 1
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: context)
        
        let scale = height / boundingBox.height
        return ceil(boundingBox.width * scale)
    }
}
