//
//  NineButtonView.swift
//  SimpleWatermark
//
//  Created by lex on 28/07/16.
//  Copyright © 2016 alcherk. All rights reserved.
//

import Cocoa

enum TextPosition {
    case UpperLeft
    case Upper
    case UpperRight
    case Left
    case Center
    case Right
    case BottomLeft
    case Bottom
    case BottomRight
    
    static func positionIn(size:CGSize, pos: NSPoint) -> TextPosition {
        var b:TextPosition = .Center
        
        let thirdHeight: Int = Int(size.height / 3)
        let thirdWidth: Int = Int(size.width / 3)
        
        switch (Int(pos.x), Int(pos.y)) {
        case (0..<thirdWidth, 0..<thirdHeight) :
            b = .BottomLeft
        case (thirdWidth...(thirdWidth * 2), 0...thirdHeight) :
            b = .Bottom
        case ((thirdWidth * 2)...Int(size.width), 0...thirdHeight) :
            b = .BottomRight
        case (0...thirdWidth, thirdHeight...(thirdHeight * 2)) :
            b = .Left
        case ((thirdWidth * 2)...Int(size.width), thirdHeight...(thirdHeight * 2)) :
            b = .Right
        case (0...thirdWidth, (thirdHeight * 2)...Int(size.height)) :
            b = .UpperLeft
        case (thirdWidth...(thirdWidth * 2), (thirdHeight * 2)...Int(size.height)) :
            b = .Upper
        case ((thirdWidth * 2)...Int(size.width), (thirdHeight * 2)...Int(size.height)) :
            b = .UpperRight
        default:
            b = .Center
        }
        
        return b
    }
    
    static func rectForButton(button:TextPosition, size:CGSize) -> NSRect {
        var rect:CGRect = NSZeroRect
        
        let thirdHeight: CGFloat = CGFloat(size.height / 3)
        let thirdWidth: CGFloat = CGFloat(size.width / 3)
        
        switch button {
        case .BottomLeft:
            rect = NSMakeRect(0, 0, thirdWidth, thirdHeight)
        case .Bottom:
            rect = NSMakeRect(thirdWidth, 0, thirdWidth, thirdHeight)
        case .BottomRight:
            rect = NSMakeRect(thirdWidth * 2, 0, size.width, thirdHeight)
        case .Left:
            rect = NSMakeRect(0, thirdHeight, thirdWidth, thirdHeight)
        case .Center:
            rect = NSMakeRect(thirdWidth, thirdHeight, thirdWidth, thirdHeight)
        case .Right:
            rect = NSMakeRect(thirdWidth * 2, thirdHeight, size.width, thirdHeight)
        case .UpperLeft:
            rect = NSMakeRect(0, thirdHeight * 2, thirdWidth, size.height)
        case .Upper:
            rect = NSMakeRect(thirdWidth, thirdHeight * 2, thirdWidth, size.height)
        case .UpperRight:
            rect = NSMakeRect(thirdWidth * 2, thirdHeight * 2, size.width, size.height)
        }
        
        return rect
    }
}

protocol NineButtonDelegate {
    func buttonClicked(button: TextPosition)
}

class NineButtonView: NSView {
    
    let color = NSColor(withHex: 0xB6B6B6FF)
    let gridColor = NSColor(withHex: 0xC6C6C6FF)
    
    var selectedButton: TextPosition = .Center
    var delegate:NineButtonDelegate? = nil
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        
        setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    func setupView() {
        self.wantsLayer = true
        self.layer!.masksToBounds = true
        self.setBorder(width: 1.0, color: color)
        self.layer!.cornerRadius = 5.0
        
        self.layer?.backgroundColor = NSColor(withHex: 0xE6E6E6FF).cgColor
    }
    
    func drawLine(startPoint: NSPoint, endPoint: NSPoint) {
        let line = NSBezierPath()
        line.move(to: startPoint)
        line.line(to: endPoint)
        line.lineWidth = 0.5
        gridColor.set()
        line.stroke()
    }
    
    func drawSelection() {
        NSColor.blue.set()
        let selectionRect = TextPosition.rectForButton(button: self.selectedButton, size: self.bounds.size)
        selectionRect.fill()
    }
    
    func drawIcons() {
        
    }
    
    override func draw(_ dirtyRect: NSRect) {
        
        let step = dirtyRect.width / 3
        
        NSGraphicsContext.current?.shouldAntialias = false
        
        for i in 1...2 {
            drawLine(startPoint: NSMakePoint(step * CGFloat(i), 0), endPoint: NSMakePoint(step * CGFloat(i), dirtyRect.height))
            drawLine(startPoint: NSMakePoint(0, step * CGFloat(i)), endPoint: NSMakePoint(dirtyRect.height, step * CGFloat(i)))
        }
        
        drawSelection()
        
        drawIcons()
        
        super.draw(dirtyRect)
    }
    
    override func mouseDown(with event : NSEvent) {
        let pos = self.convert(event.locationInWindow, from: self.window?.contentView)
        let size = self.bounds.size
        
        selectedButton = TextPosition.positionIn(size: size, pos: pos)
        
        delegate?.buttonClicked(button: selectedButton)
        
        self.needsDisplay = true
        
        NSLog("left mouse: \(selectedButton)")
        
    }
    
}
