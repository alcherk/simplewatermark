//
//  PhotoCell.swift
//  SimpleWatermark
//
//  Created by Aleksey Cherkasskiy on 13/05/2019.
//  Copyright © 2019 alcherk. All rights reserved.
//

import Cocoa

class PhotoCell: NSCollectionViewItem {
    @IBOutlet var previewImage: NSImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
