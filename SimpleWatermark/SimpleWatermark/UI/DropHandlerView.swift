//
//  DropHandlerView.swift
//  SimpleWatermark
//
//  Created by lex on 17/07/16.
//  Copyright © 2016 alcherk. All rights reserved.
//

import Cocoa

protocol MouseEventDelegate: class {
    func leftMouseDown(locationInWindow: NSPoint)
    func mouseDragged(delta: NSPoint)
    func mouseUp(locationInWindow: NSPoint)
}

class DropHandlerView: NSView {
    
    var mouseDelegate: MouseEventDelegate?

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        // Declare and register an array of accepted types
        
        if #available(OSX 10.13, *) {
            registerForDraggedTypes([NSPasteboard.PasteboardType.URL])
        } else {
            // Fallback on earlier versions
        }
//        registerForDraggedTypes([kUTTypeURL as String])
//        if #available(OSX 10.13, *) {
//            registerForDraggedTypes([NSPasteboard.PasteboardType.fileURL, NSPasteboard.PasteboardType.URL, NSPasteboard.PasteboardType.tiff])
//        } else {
//            // Fallback on earlier versions
//        }
    }
    
    let fileTypes = ["jpg", "jpeg", "bmp", "png", "gif"]
    var fileTypeIsOk = false
    var droppedFilePaths: [String] = []

    
    override func draw(_ dirtyRect: NSRect) {
        super.draw(dirtyRect)

        // Drawing code here.
    }
    
    override func mouseDown(with theEvent: NSEvent) {
        mouseDelegate?.leftMouseDown(locationInWindow: theEvent.locationInWindow)
    }
    
    override func mouseDragged(with event: NSEvent) {
        mouseDelegate?.mouseDragged(delta: NSPoint(x: event.deltaX, y: event.deltaY))
    }
    
    override func mouseUp(with event: NSEvent) {
        mouseDelegate?.mouseUp(locationInWindow: event.locationInWindow)
    }
}

extension DropHandlerView {
    
    override func draggingEntered(_ sender: NSDraggingInfo) -> NSDragOperation {
        if checkExtension(drag: sender) {
            fileTypeIsOk = true
            return .copy
        } else {
            fileTypeIsOk = false
            return []
        }
    }
    
    override func draggingUpdated(_ sender: NSDraggingInfo) -> NSDragOperation {
        if fileTypeIsOk {
            return .copy
        } else {
            return []
        }
    }
    
    override func performDragOperation(_ sender: NSDraggingInfo) -> Bool {
        let filteringOptions = [NSPasteboard.ReadingOptionKey.urlReadingContentsConformToTypes:NSImage.imageTypes]
        if let urls = sender.draggingPasteboard.readObjects(forClasses: [NSURL.self], options:filteringOptions) as? [URL], urls.count > 0 {
            for url in urls {
                let ip = url.path
                guard !droppedFilePaths.contains(ip) else { continue }
                if checkOneFile(file: url) {
                    droppedFilePaths.append(ip)
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NewImagesAdded"), object: nil)
            return true
        }
//        if #available(OSX 10.13, *) {
//            if let board = sender.draggingPasteboard.propertyList(forType: NSPasteboard.PasteboardType.URL) as? NSArray,
//                let imagePaths = board as? [String] {
//                for ip in imagePaths {
//                    guard !droppedFilePaths.contains(ip) else { continue }
//                    if checkOneFile(file: NSURL(fileURLWithPath: ip)) {
//                        droppedFilePaths.append(ip)
//                    }
//                }
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "NewImagesAdded"), object: nil)
//                return true
//            }
//        } else {
//            // Fallback on earlier versions
//        }
        return false
    }
    
    func checkOneFile(file: URL) -> Bool {
        let fileExtension = file.pathExtension.lowercased
        return fileTypes.contains(fileExtension())
    }
    
    func checkExtension(drag: NSDraggingInfo) -> Bool {
        if #available(OSX 10.13, *) {
            let pasteboard = drag.draggingPasteboard
            let filteringOptions = [NSPasteboard.ReadingOptionKey.urlReadingContentsConformToTypes:NSImage.imageTypes]
            if pasteboard.canReadObject(forClasses: [NSURL.self], options: filteringOptions) {
                return true
            }
//            if let board = drag.draggingPasteboard.propertyList(forType: NSPasteboard.PasteboardType.fileURL) as? NSArray,
//                let path = board[0] as? String {
//                let url = NSURL(fileURLWithPath: path)
//                return checkOneFile(file: url)
//            }
        } else {
            // Fallback on earlier versions
        }
        return false
    }
}
