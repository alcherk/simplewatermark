//
//  NSImageView+Utils.swift
//  SimpleWatermark
//
//  Created by lex on 26/07/16.
//  Copyright © 2016 alcherk. All rights reserved.
//

import Cocoa

extension NSImageView {
    
    var photoReduction: CGFloat {
        let size = self.image!.size
        let iFrame = self.bounds
        if (NSWidth(iFrame) > size.width && NSHeight(iFrame) > size.height) {
            return 1.0 // it fits
        }
        else {
            // one leg of the photo doesn't fit - the smallest ratio rules
            let xRatio = NSWidth(iFrame) / size.width
            let yRatio = NSHeight(iFrame) / size.height
            return min(xRatio, yRatio)
        }
    }
    
    var photoRectInImageView: NSRect {
        let size = self.image!.size
        let iBounds = self.bounds
        let reduction = self.photoReduction
        var photoRect = NSZeroRect
    
        photoRect.size.width = floor(size.width * reduction + 0.5)
        photoRect.size.height = floor(size.height * reduction + 0.5)
        photoRect.origin.x = floor((iBounds.size.width - photoRect.size.width) / 2.0 + 0.5)
        photoRect.origin.y = floor((iBounds.size.height - photoRect.size.height) / 2.0 + 0.5)
        return photoRect
    }
}
