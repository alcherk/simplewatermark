//
//  NSView+extensions.swift
//  SimpleWatermark
//
//  Created by lex on 17/07/16.
//  Copyright © 2016 alcherk. All rights reserved.
//

import Cocoa

extension NSView {
    
    var backgroundColor: NSColor? {
        get {
            if let colorRef = self.layer?.backgroundColor {
                return NSColor(cgColor: colorRef)
            } else {
                return nil
            }
        }
        set {
            self.wantsLayer = true
            self.layer?.backgroundColor = newValue?.cgColor
        }
    }
    
    
    class func nibName() -> String {
        return "\(self)".components(separatedBy: ".").first ?? ""
    }
    
    public class func loadFromNibFile() -> Self? {
        return fromNib()
    }
    
    public class func fromNib<T: NSView>() -> T? {
        var viewArray:NSArray?
        Bundle.main.loadNibNamed(self.nibName(), owner: self, topLevelObjects: &viewArray)
        
        for v in viewArray! {
            if let view = v as? T {
                return view
            }
        }
        return nil
    }
    
    func setBorder(width: CGFloat, color: NSColor) {
        self.wantsLayer = true
        self.layer?.borderWidth = width
        self.layer?.borderColor = color.cgColor
    }
    
}
