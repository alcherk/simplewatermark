//
//  NSColor+extensions.swift
//  SimpleWatermark
//
//  Created by lex on 17/07/16.
//  Copyright © 2016 alcherk. All rights reserved.
//

import Cocoa

extension NSColor {
    /**
     The six-digit hexadecimal representation of color with alpha of the form #RRGGBBAA.
     
     - parameter withHex: Eight-digit hexadecimal value.
     */
    public convenience init(withHex: UInt32) {
        let divisor = CGFloat(255)
        let red     = CGFloat((withHex & 0xFF000000) >> 24) / divisor
        let green   = CGFloat((withHex & 0x00FF0000) >> 16) / divisor
        let blue    = CGFloat((withHex & 0x0000FF00) >>  8) / divisor
        let alpha   = CGFloat( withHex & 0x000000FF       ) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
