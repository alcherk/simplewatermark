//
//  MainViewController.swift
//  SimpleWatermark
//
//  Created by lex on 17/07/16.
//  Copyright © 2016 alcherk. All rights reserved.
//

import Cocoa
import QuartzCore

class MainViewController: NSViewController {
    
    
    @IBOutlet var fileDropView: DropHandlerView!
    @IBOutlet var dropFileLabel: NSTextField!
    @IBOutlet var sideToolsView: NSView!
    @IBOutlet var filesListView: NSView!
    @IBOutlet var fileListToolsView: NSView!
    @IBOutlet var imagesCollectionView: NSCollectionView!
    @IBOutlet var previewImageView: NSImageView!
    @IBOutlet var dropFilesLabel: NSTextField!
    
    private var imageLayer = CALayer()
    fileprivate var textLayer: WatermarkLayer?
    private var currentImage: NSImage?
    
    fileprivate var isMoving = false
    fileprivate var lastOrigin: CGPoint = .zero

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupImageCollectionView()
        previewImageView.unregisterDraggedTypes()

        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.updateImageList), name: NSNotification.Name(rawValue: "NewImagesAdded"), object: nil)
        
        fileDropView.mouseDelegate = self
    }
    
    override func viewWillAppear() {
        setupStyles()
        view.addObserver(self, forKeyPath: #keyPath(NSView.bounds), options: .new, context: nil)

        super.viewWillAppear()
    }
    
    override func viewWillDisappear() {
        view.removeObserver(self, forKeyPath: #keyPath(NSView.bounds))
        super.viewWillDisappear()
    }
    
    override func viewWillLayout() {
        super.viewWillLayout()
        imageLayer.frame = previewImageView.bounds
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let objectView = object as? NSView,
            objectView === view,
            keyPath == #keyPath(NSView.bounds) {
            imageLayer.frame = objectView.bounds
            textLayer?.frame.origin = objectView.bounds.origin
        }
    }
    
    func setupStyles() {
        fileDropView.wantsLayer = true
        fileDropView.layer?.backgroundColor = NSColor(withHex: 0x6F6F6FFF).cgColor
        dropFileLabel.textColor = NSColor(withHex: 0xF7F7F7FF)
        dropFileLabel.drawsBackground = true
//        dropFileLabel.backgroundColor = NSColor(withHex: 0x1F1F1FFF)
        dropFileLabel.cell?.backgroundStyle = .raised
        
        sideToolsView.wantsLayer = true
        sideToolsView.layer?.backgroundColor = NSColor(withHex: 0xF2F2F2FF).cgColor
        
        filesListView.wantsLayer = true
        filesListView.layer?.backgroundColor = NSColor(withHex: 0x333333FF).cgColor
        
        fileListToolsView.wantsLayer = true
        fileListToolsView.layer?.backgroundColor = NSColor(withHex: 0x111111FF).cgColor
        
        dropFileLabel.stringValue = NSLocalizedString("Drop files here", comment: "")
    }
    
    func setupImageCollectionView() {
        let flowLayout = NSCollectionViewFlowLayout()
        flowLayout.itemSize = NSSize(width: 150.0, height: 100.0)
        flowLayout.sectionInset = NSEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)
        flowLayout.minimumInteritemSpacing = 20.0
        flowLayout.minimumLineSpacing = 20.0
        flowLayout.scrollDirection = .horizontal
        imagesCollectionView.collectionViewLayout = flowLayout
        view.wantsLayer = true
        imagesCollectionView.layer?.backgroundColor = NSColor.black.cgColor
        
        let nib = NSNib(nibNamed: "PhotoCell", bundle: nil)!
        imagesCollectionView.register(nib, forItemWithIdentifier: NSUserInterfaceItemIdentifier(rawValue: "PhotoCell"))

        imagesCollectionView.dataSource = self
        imagesCollectionView.delegate = self
    }
    
    func hasImageSelected() -> Bool {
        return imagesCollectionView.selectionIndexes.count > 0
    }
    
    func selectFirstImage() {
        imagesCollectionView.selectItems(at: [NSIndexPath.init(forItem: 0, inSection: 0) as IndexPath], scrollPosition: NSCollectionView.ScrollPosition.init(rawValue: 0))
        updatePreviewWithImageIndex(index: 0)
    }
    
    func drawWatermarkPreview() {
        textLayer?.removeFromSuperlayer()
        let fontName: CFString = "Courier" as CFString
        let font = CTFontCreateWithName(fontName, 70.0, nil)

        textLayer = WatermarkLayer.make(with: "Simple Watermark", font: font)
        textLayer?.autoresizingMask = [CAAutoresizingMask.layerMaxXMargin, CAAutoresizingMask.layerMinYMargin]

        textLayer?.foregroundColor = NSColor.red.cgColor
        textLayer?.alignmentMode = CATextLayerAlignmentMode.left
        textLayer?.frame.origin = lastOrigin
        
        if let textLayer = textLayer {
            imageLayer.addSublayer(textLayer)
        }
        textLayer?.display()
    }
    
    func updatePreviewWithImageIndex(index: Int) {
        guard index < self.fileDropView.droppedFilePaths.count else { return }

        currentImage = NSImage(contentsOfFile: self.fileDropView.droppedFilePaths[index])
        drawLayerWith(image: currentImage)
        
        dropFilesLabel.isHidden = true
        
        drawWatermarkPreview()
    }
    
    private func drawLayerWith(image: NSImage?) {
        imageLayer.removeFromSuperlayer()
        imageLayer.frame = previewImageView.bounds
        imageLayer.contentsGravity = .resizeAspectFill
        imageLayer.autoresizingMask = [CAAutoresizingMask.layerWidthSizable, CAAutoresizingMask.layerHeightSizable]
        imageLayer.contents = image
        previewImageView.layer?.addSublayer(imageLayer)
    }
    
    @objc func updateImageList() {
        self.imagesCollectionView.reloadData()
        if self.fileDropView.droppedFilePaths.count > 0 && !hasImageSelected() {
            selectFirstImage()
        }
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

}

extension MainViewController: NSCollectionViewDataSource, NSCollectionViewDelegate {    
    
    func numberOfSectionsInCollectionView(collectionView: NSCollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.fileDropView.droppedFilePaths.count
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem  {
        let item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "PhotoCell"), for: indexPath) as! PhotoCell

        let image = NSImage(contentsOfFile: self.fileDropView.droppedFilePaths[indexPath.item])
        item.previewImage.image = image

        return item
    }
        
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        guard let indexPath = indexPaths.first else { return }

        updatePreviewWithImageIndex(index: indexPath.item)
    }
}

extension MainViewController: MouseEventDelegate {
    func leftMouseDown(locationInWindow: NSPoint) {
    }
    
    func mouseDragged(delta: NSPoint) {
        CATransaction.begin()
        defer {
            CATransaction.commit()
        }
        CATransaction.setDisableActions(true)
        
        var newOrigin = textLayer?.frame.origin ?? .zero
        newOrigin.x += delta.x
        newOrigin.y -= delta.y
        textLayer?.frame.origin = newOrigin
        isMoving = true
        lastOrigin = textLayer?.frame.origin ?? .zero
    }
    
    func mouseUp(locationInWindow: NSPoint) {
        guard !isMoving else {
            isMoving = false
            return
        }
        let converted = fileDropView.convert(locationInWindow, from: view)
        textLayer?.position = converted
        lastOrigin = textLayer?.frame.origin ?? .zero
    }
}
